import { Component, OnInit, Input } from '@angular/core';
import { Usuario } from 'src/app/modelos/usuario'; //Modelo a emplear
import { Router, ActivatedRoute } from '@angular/router'; //Dependencias para navegar entre rutas
import { UsuarioServicio} from '../../services/usuarios.service'; //Servicos a consumir

//Componente de visualizar
@Component({
  selector: 'app-visualizar',
  templateUrl: './visualizar.component.html',
  styleUrls: ['./visualizar.component.css'],
  providers: [UsuarioServicio]
})
export class VisualizarComponent implements OnInit {

  public datosUsuario: Usuario; //Se crea el modelo a usar

  constructor(
    private _servicoUsuario: UsuarioServicio,
    private _route: ActivatedRoute,
    private _router: Router
  ) {
      this.datosUsuario = new Usuario('','','','','','','','','','',0,0,0); //Se incializa el modelo
   }
   //Funcion que se ejecuta al cargar la pagina
  ngOnInit(): void {
    //Capturamos el paramatreo datos que viene por URL
    var datos = this._route.snapshot.paramMap.get('datos');
    //Se consume el servicoio de buscar pasando el _id del usuario que se encuentra inmerso en datos
    this._servicoUsuario.buscar(datos).subscribe(
      response =>{
        //Si la respuesta es exitosa
        if(response.status == 'success'){
          //Rellena los campos del formulario para su visualizacion
          this.datosUsuario.nombreCompleto= response.usuario.datosPersonales.nombreCompleto;
          this.datosUsuario.identificacion= response.usuario.datosPersonales.identificacion;
          this.datosUsuario.celular= response.usuario.datosPersonales.celular;
          this.datosUsuario.correo= response.usuario.datosPersonales.correo;
          this.datosUsuario.contrasena= response.usuario.datosPersonales.contraseña;
          this.datosUsuario.departamento= response.usuario.datosResidencia.departamento;
          this.datosUsuario.ciudad= response.usuario.datosResidencia.ciudad;
          this.datosUsuario.barrio= response.usuario.datosResidencia.barrio;
          this.datosUsuario.direccion= response.usuario.datosResidencia.direccion;
          this.datosUsuario.salario= response.usuario.datosFinancieros.salario;
          this.datosUsuario.otrosIngresos= response.usuario.datosFinancieros.otrosIngresos  ;
          this.datosUsuario.gastosMensuales= response.usuario.datosFinancieros.gastosMensuales;
          this.datosUsuario.gastosFinancieros= response.usuario.datosFinancieros.gastosFinancieros;
        }else{
          //Si no, se muestra el mensaje de error
          alert(response.message);
        }
      },
      error => {
        ////Si no, se muestra el mensaje de error
       alert(error.error.message);
      }
    );
  
  }
  //Funcion para cerrar sesion
  cerrar(): void {
    //Se muestra mensaje de cerrar sesion
    alert('Cerrar sesion');
    //Redireccionamos a la página de inicio
    this._router.navigate(['/']);
  }

}
