import { Component, OnInit } from '@angular/core';
import { UsuarioServicio} from '../../services/usuarios.service'; //Servicio a consumir
import { Usuario } from '../../modelos/usuario'; //Modelo usado
import { Router, ActivatedRoute } from '@angular/router'; //Dependencia para navegar entre rutas

//Componente para registrar
@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.css'],
  providers: [UsuarioServicio]
})
export class RegistrarComponent implements OnInit {

  public usuarioRegistrar: Usuario; //Se crea el modelo
  public valida: any; //Objeto para validaciones
  
  constructor(
    private _servicoRegistrar: UsuarioServicio,
    private _route: ActivatedRoute,
    private _router: Router
  ) {
      this.usuarioRegistrar = new Usuario('','','','','','','','','','',0,0,0); //Se inicializa el Modelo a enviar al servidor
      this.valida = {
        contrasena: '',
        rangos: '',
        validaRango:''
      } //Se inicaliza el objeto de validaciones
   }

   //Funciones que se ejecutan al inicializar la pantalla
  ngOnInit(): void {
  }
  //Funcion para registrar un usuario
  registrarUsuario(){
    //Se consume el servicoio para registrar, enviando el modelo para la validacion en el servidor
    this._servicoRegistrar.registrar(this.usuarioRegistrar).subscribe(
      response =>{
      //Si la respuesta es exitosa
      if(response.status == 'success'){
        //Se muestra un mensaje de éxito
        alert("Usuario Registrado Con Éxito");
        //Realiza un redireccionamiento a la pagina del login
        this._router.navigate(['/login']);
      }else{
        //Si no, se muestra el mensaje de error
        alert(response.message);
      }
      },
      error => {
        //Si no, se muestra el mensaje de error
       alert(error.error.message);
      }
    );
  }
  //Funcion  para calcular el rango de salarios
  rangoSalario(salario:any){
    //Se consume el servicio para obtener los rangos de acuerdo al salario enviado
    this._servicoRegistrar.rangos(salario).subscribe(
      response =>{
      //Si la respuesta es exitosa
      if(response.status == 'success'){
        //Se setean los valores de los rangos de salario recibidos
        this.valida.rangos = [response.rangos.a,response.rangos.b,response.rangos.c];
      }else{
        //Si no, se muestra el mensaje de error
        alert(response.message);
      }
      },
      //Si no, se muestra el mensaje de error
      error => {
       alert(error.error.message);
      }
    );
  }
}
