import { Component, OnInit } from '@angular/core';
import { UsuarioLogin} from '../../modelos/usuarioLogin'; //Modelo usado
import { UsuarioServicio} from '../../services/usuarios.service'; //Servicio a usar
import { Router, ActivatedRoute } from '@angular/router'; //Dependencia empleada para la navegacion entre rutas

//Componente de Login - Iniciar sesion
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UsuarioServicio]
})
export class LoginComponent implements OnInit {

  //Objeto a enviar al servidor
  public usuarioLogin: UsuarioLogin;
  
  constructor(
    private _servicoLogin: UsuarioServicio,
    private _route: ActivatedRoute,
    private _router: Router
  ) {
      this.usuarioLogin = new UsuarioLogin('','');
   }

  //Funcionalidades que se ejecutan al cargar la pagina
  ngOnInit(): void {
     
  }
  //Funcion para inciar sesio estando registrada previamente
  iniciarSesion(){
    //Se consume el servicio de login enviando el parametro para su validación
    this._servicoLogin.login(this.usuarioLogin).subscribe(
      response =>{
        //Si es exitosa
        if(response.status == 'success'){
          //visualizamos lo datos de la persona en el componente visualizar
          this._router.navigate(['/visualizar/'+response.usuarioLogin[0]._id]); //Se envia el _id del ususario por medio de la ruta
        }else{
          //Si no, mostramos el mensaje de error
          alert(response.message);
        }
      },
      error => {
       //Se muestra el error
       alert(error.error.message);
      }
    );
  }

}
