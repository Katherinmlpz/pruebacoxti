import { ModuleWithProviders} from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InicioComponent } from './componentes/inicio/inicio.component'; //Importacion del componente a dirigir
import { LoginComponent } from './componentes/login/login.component'; //Importacion del componente a dirigir
import { RegistrarComponent } from './componentes/registrar/registrar.component'; //Importacion del componente a dirigir
import { VisualizarComponent } from './componentes/visualizar/visualizar.component';  //Importacion del componente a dirigir

//Se crea el archivo con la configuracion de las rutas del aplicativo
const appRoutes: Routes = [
//Se crea el enlace entre la ruta y el componente
    {path: '', component: InicioComponent},
    {path: 'inicio', component: InicioComponent},
    {path: 'login', component: LoginComponent},
    {path: 'registrar', component: RegistrarComponent},
    {path: 'visualizar/:datos', component: VisualizarComponent},

];
/* Se establece como servicios */
export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders<any> = RouterModule.forRoot(appRoutes);

