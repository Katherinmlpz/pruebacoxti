import { Injectable } from '@angular/core'; //Dependencia para injectar los servicios en los componentes
import { HttpClient, HttpHeaders } from '@angular/common/http'; //Dependencia para hacer peticiones Http
import { Observable } from 'rxjs'; //Dependencia para la comunicacion entre las partes de la aplicacion
import { Global } from './global'; //Dependencia con la URL del API

//Se crea la injeccion de dependencias
@Injectable()
export class UsuarioServicio{
  
    public url: String; //Definimos el parametro de url

    constructor(
        private _http: HttpClient //Se crea el parametro para las peticiones Http
    ){
        this.url = Global.url; //Se setea la url del API
    }
    //Se crea la conexion con el servicio de login en el API
    login(datos:any):Observable<any>{
        //Obtenemos los datos del API
        var datosIngreso = this._http.post(this.url+'login', datos);
        //Devolvemos la respuesta del servidor
        return datosIngreso;
    }
    //Se crea la conexion con el servicio de registrar en el API
    registrar(datos:any):Observable<any>{
        //Obtenemos los datos del API
        var datosRegistro = this._http.post(this.url+'registrar-usuario', datos);
        //Devolvemos la respuesta del servidor
        return datosRegistro;
    }
    //Se crea la conexion con el servicio de buscar en el API
    buscar(datos:any):Observable<any>{
        //Obtenemos los datos del API
        var datosRegistro = this._http.get(this.url+'/buscar/'+datos);
        //Devolvemos la respuesta del servidor
        return datosRegistro;
    }
    //Se crea la conexion con el servicio de rangos en el API
    rangos(datos:any):Observable<any>{
        //Obtenemos los datos del API
        var rangos = this._http.get(this.url+'/salario/'+datos);
        //Devolvemos la respuesta del servidor
        return rangos;
    }
}