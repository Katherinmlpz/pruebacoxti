export class Usuario{
//Modelo de usuario
    constructor(
       public nombreCompleto: String,
       public identificacion: String,
       public celular: String,
       public correo: String,
       public contrasena: String,
       public departamento: String,
       public ciudad: String,
       public barrio: String,
       public direccion: String,
       public salario: String,
       public otrosIngresos: Number,
       public gastosMensuales: Number,
       public gastosFinancieros: Number
    ){}
}