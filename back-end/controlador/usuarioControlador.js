'use strict'

var Usuario = require('../modelo/usuario'); //Se importa el modelo a usar
//Se crea el controlador
var usuarioControlador = {
    //Se crea el servicio de registrar usuarios
    registrar: (req, res) => {
        // Recoger parametros por post
        var params = req.body;
        //Buscamos el correo en la base de datos para validar de que no esté registrado
        Usuario.find({ "datosPersonales.correo": params.correo })
            .exec((err, usuarioLogin) => {
                if (err) {
                    return res.status(500).send({
                        status: 'error',
                        message: 'Error en la petición !!!'
                    });
                }

                if (!usuarioLogin || usuarioLogin.length <= 0) {
                    //Crear el objeto a guardar
                    var usuario = new Usuario();

                    // Asignar valores
                    usuario.datosPersonales.nombreCompleto = params.nombreCompleto;
                    usuario.datosPersonales.identificacion = params.identificacion;
                    usuario.datosPersonales.celular = params.celular;
                    usuario.datosPersonales.correo = params.correo;
                    usuario.datosPersonales.contraseña = params.contrasena;

                    usuario.datosResidencia.departamento = params.departamento;
                    usuario.datosResidencia.ciudad = params.ciudad;
                    usuario.datosResidencia.barrio = params.barrio;
                    usuario.datosResidencia.direccion = params.direccion;

                    usuario.datosFinancieros.salario = params.salario;
                    usuario.datosFinancieros.otrosIngresos = params.otrosIngresos;
                    usuario.datosFinancieros.gastosMensuales = params.gastosMensuales;
                    usuario.datosFinancieros.gastosFinancieros = params.gastosFinancieros;

                    // Guardar el usuario
                    usuario.save((err, usuarioGuardado) => {

                        if (err || !usuarioGuardado) {
                            return res.status(404).send({
                                status: 'error',
                                message: 'El usuario no se ha guardado !!!'
                            });
                        }
                        // Devolver una respuesta 
                        return res.status(200).send({
                            status: 'success',
                            usuario: usuarioGuardado
                        });
                    });
                } else {
                    return res.status(404).send({
                        status: 'error',
                        message: 'El correo ya se encuentra registrado'
                    });
                }
            });
    },
    //Se crea el servicio de login de usuarios
    login: (req, res) => {
        // Recoger parametros por post
        var datosSesion = req.body;
        // Se busca el usuario con correo y contraseña en la base de datos
        Usuario.find({
            "datosPersonales.correo": datosSesion.correo,
            "datosPersonales.contraseña": datosSesion.contrasena
        }).exec((err, usuarioLogin) => {
                if (err) {
                    return res.status(500).send({
                        status: 'error',
                        message: 'Error en la petición !!!'
                    });
                }

                if (!usuarioLogin || usuarioLogin.length <= 0) {
                    return res.status(404).send({
                        status: 'error',
                        message: 'No existe usuario !!!'
                    });
                }

                return res.status(200).send({
                    status: 'success',
                    usuarioLogin
                });
            });
    },
    //Se crea el servicio de generar rangos de salario del usuario
    generarRangos: (req, res) => {
        // Recoger parametros por get
        var salario = parseInt(req.params.datos);
        //Se crea el array con los rangos de salario
        var rangos = [];
        // Se crea el JSON con los rangos
        var r = {
            a: '',
            b: '',
            c: ''
        };
        //Se generan los rangos de salario y se insertan en el array de rangos
        rangos.push((salario - (salario * 0.25)) + ' - ' + ((salario * 0.25) + salario));
        rangos.push((salario + (salario * 0.25)) + ' - ' + (salario + (salario * 1.25)));
        rangos.push((salario + (salario * 1.25)) + ' - ' + (salario + (salario * 2.25)));
        //Se barajean  los datos del array
        rangos.sort(function () {
            return Math.random() - 0.5
        });
        //Se guardan los rangos de los salario en el JSON r
        r.a = rangos[0];
        r.b = rangos[1];
        r.c = rangos[2];
        //Se envian los rangos generados
        return res.status(200).send({
            status: 'success',
            rangos: r
        });
    },
    //Se crea el servicio de buscar usuario por _id
    buscarById: (req, res) => {
        // Recoger parametros por get
        var id = req.params.datos;
        // Se busca el usuario con el _id en la base de datos
        Usuario.findById(id, (err, usuario) => {
            if (err || !usuario) {
                return res.status(404).send({
                    status: 'error',
                    message: 'No existe el usuario !!!'
                });
            }

            // Devolverlo en json
            return res.status(200).send({
                status: 'success',
                usuario
            });
        });
    }
};  // fin controlador

module.exports = usuarioControlador;