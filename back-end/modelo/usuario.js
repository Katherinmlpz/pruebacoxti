'use strict'
//Creacion del modelo de datos para el esquema en base de datos

var mongoose = require('mongoose'); //Se importa el controlador de la base de datos
var Schema = mongoose.Schema; //Se crea el esquema de la base de datos
//Se crea la estructura del esquema de base datos
var DatosUsuarioSchema = Schema({
    datosPersonales: {
        nombreCompleto: String,
        identificacion: String,
        celular: String,
        correo: String,
        contraseña: String
    },

    datosResidencia: {
        departamento: String,
        ciudad: String,
        barrio: String,
        direccion: String
    },

    datosFinancieros: {
        salario: String,
        otrosIngresos: Number,
        gastosMensuales: Number,
        gastosFinancieros: Number
    }
});

module.exports = mongoose.model('DatoUsuario', DatosUsuarioSchema);
// usuario --> guarda documentos de este tipo y con estructura dentro de la colección