'use strict'
//Se configura el parametro que contiene la URL de conexión con la base de datos
const uri = "mongodb+srv://admin:root@baseprueba.a6flc.mongodb.net/datosPruebaIngreso";
var mongoose = require('mongoose'); //Se importa el controlador para la base de datos
var app = require('./app'); //Se importa el app
var port = 3900; //Se establece el puerto para el inicio del servidor

mongoose.set('useFindAndModify', false);
mongoose.Promise = global.Promise;
//Se crea la conexión con la base de datos
mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true }).then(()=>{
    console.log('La conexión se ha hecho correctamente!!');
    //Creación del servidor, puerto de escucha http
    app.listen(port, () =>{
        console.log('Servidor corriendo en http://localhost:'+ port);
    });
});

        