'use strict'

var express = require('express');
var usuarioControlador = require('../controlador/usuarioControlador'); //Se importa el controlador del servidor

var rutas = express.Router(); //Se importan las rutas

// Rutas del servidor para el consumo de servicios
rutas.post('/registrar-usuario', usuarioControlador.registrar);
rutas.post('/login', usuarioControlador.login);
rutas.get('/salario/:datos?', usuarioControlador.generarRangos);
rutas.get('/buscar/:datos?', usuarioControlador.buscarById);

module.exports = rutas;