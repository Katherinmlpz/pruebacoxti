# pruebaCOXTI

#La prueba debe ser subida a un repositorio público de GitLab, agregando a @lbenavides.coxti y @edissonAlvarez como colaboradores e incluir un archivo README donde se indique qué fue lo que hizo, que estrategias empleó y qué dificultades para el desarrollo de la prueba.

#Qué se hizo
    #Back-end
        Se realizo la creacion del backend con Node.js con un modelo MVC
        el IDE empleado fue VSCode
        Base de datos empleada: MongoDB -> Se especifica las dificultades presentadas en #Dificultades
        Se crea el esquema para la base de datos con la siguiente estructura:

        datosPersonales: {
        nombreCompleto: String,
        identificacion: String,
        celular: String,
        correo: String,
        contraseña: String
        },

        datosResidencia: {
            departamento: String,
            ciudad: String,
            barrio: String,
            direccion: String
        },

        datosFinancieros: {
            salario: String,
            otrosIngresos: Number,
            gastosMensuales: Number,
            gastosFinancieros: Number
        }

        {
            "name": "back-end",
            "version": "1.0.0",
            "description": "Prueba de ingreso COXTI",
            "main": "index.js",
            "scripts": {
                "start": "nodemon index.js",
                "test": "echo \"Error: no test specified\" && exit 1"
            },
            "repository": {
                "type": "git",
                "url": "git+https://gitlab.com/Katherinmlpz/pruebacoxti.git"
            },
            "author": "Katherin Muñoz López",
            "license": "ISC",
            "bugs": {
                "url": "https://gitlab.com/Katherinmlpz/pruebacoxti/issues"
            },
            "homepage": "https://gitlab.com/Katherinmlpz/pruebacoxti#readme",
            "dependencies": {
                "body-parser": "^1.19.0",
                "connect-multiparty": "^2.2.0",
                "express": "^4.17.1",
                "mongoose": "^5.12.13",
                "validator": "^13.6.0"
            },
            "devDependencies": {
                "nodemon": "^2.0.7"
            }
        }

        Al crear cada uno de los servicios se realizó la prueba de éstos por medio de peticiones https a través de
        postman.
        **Para la generación de rangos de salarios se hizo un API que recibe el salrio desde el front-end y se realizó una
        operacion matematica para sacar cada limite y se pueda visualizar en el front por medio de un select para que el usuario
        seleccione el rango que se acopla al salario

    #Front-end

        Se realizó el front-end con Angula en su última versión
            -> Se usó una lista de estilos por cada componente creado
            -> Se enlazó bootstrap para facilitar la vista de los objetos en cada pantalla
        Se creó cada componente por medio de la consola, aprovechando la facilidad de éste.
            -> se creó un componente para la cabecera, pie, login, registrar, visualizar datos del usuario y el inicio
        Se aprovechó el sistema de componentes para llamar de un componente padre a un componente hijo por medio del enlace
        de rutas y componentes para una pagina reactiva
        Se creó un modelo para los datos del registro de usuario y otro modelo para los datos necesarios en el login
        Se creó el archivo de service para la conexión con el backend y el consumo de los servicios

#Dificultades

    1. Se esperaba enviar la prueba el día viernes pero hubo un problema con mi equipo, el disco duro falló lo que ocasionó 
       perdida de información importante (Accesibilidad a programas para realizar la prueba, no se contaba con otro equipo para
       trabajar, entre otros), por tal motivo no se realizó.
    2. Una vez solucionado lo del el disco, se procede a hacer la instalación de los programas necesarios (Como el Node y la 
       instalación de dependencias), hubieron problemas de permisos para ejecución de script y el npm install tardab mucho 
       en instalar dependencias o simplemente no cargaba más y se quedaba congelado
    3. Lo anterior es completamente solucionable, una dificultad presentada fue el poco conocimiento a las ultimas versiones
       de angular, ya que se me dificultó el uso de una base de datos como MySQL (Relacional), hubo una pequeña curva de
       aprendizaje del framework de angular y los diferentes componentes que este tiene. Obté por MongoDB por la facilidad 
       de crear la estructura y sus ventajas en aplicaciones de pequeña escala. Esto sumado al tiempo ya perdido por el estado
       del equipo me retrasó bastante y pido disculpas por lo acontecido.
    4. En la poca experiencia no había llegado a trabajar con Node Y Angular para separar el Front del Backend, se presentó 
       una buena oportunidad de aprendizaje y presentar la prueba conexito. (Claro, a excepción de la base de datos no 
       relacional) 

Para solucionar estos inconvenientes presentados frente al conocimineto de angular se están reforzando los conocimientos por medio de un curso en Udemy para fortalecer mis habilidades.

Muchas gracias por la oportunidad!!!!!!

    
    
